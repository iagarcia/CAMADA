import os
import uuid
import time

def file_generator(size):
    with open('/data_retrieve/{}.bin'.format(uuid.uuid4()), 'wb') as target:
        chunk = 10*1024*1024
        while(size > chunk):
            target.write(os.urandom(chunk))
            size -= chunk
        target.write(os.urandom(size))
    return

start = time.time()

nFiles = 1
sizeBytes = 15*1024*1024*1024

for i in range(nFiles):
    file_generator(sizeBytes)

end = time.time()

print("\nAt the end of the calculation")
print("Processor time (in seconds):", end)
print("Time elapsed during the calculation:", end - start)


import http.server
import socketserver

PORT = 8000

handler = http.server.SimpleHTTPRequestHandler
server=socketserver.TCPServer(("", PORT), handler)
print("Server started at port 8000. Press CTRL+C to close the server.")
try:
    server.serve_forever()
except KeyboardInterrupt:
    server.server_close()
    print("Server Closed")

