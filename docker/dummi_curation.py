import requests

file = '1a74f912-ba74-4362-8c94-bada87800676.txt'
r = requests.get('http://172.19.0.2:8000/data_retrieve/{}'.format(file))
r.status_code

r.headers['content-type']
r.encoding
r.text


import requests

def download_file(url, filename):
    # NOTE the stream=True parameter below
    with requests.get(url+filename, stream=True) as r:
        r.raise_for_status()
        with open('/data_curation/{}'.format(filename), 'wb') as f:
            for chunk in r.iter_content(chunk_size=8*1024): 
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk: 
                
                f.write(chunk)
    return

filename = '1a74f912-ba74-4362-8c94-bada87800676.txt'
download_file('http://svc_retrieve:8000/data_retrieve/', filename)